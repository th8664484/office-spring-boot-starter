package com.office.exception;

/*
 * @BelongsProject: office
 * @BelongsPackage: com.office.exception
 * @Author: TongHui
 * @CreateTime: 2024-03-14 11:49
 * @Description: TODO
 * @Version: 1.0
 */
public class OfficeException extends RuntimeException{

    public OfficeException(String message){
        super(message);
    }

    public OfficeException(Throwable cause)
    {
        super(cause);
    }

    public OfficeException(String message, Throwable cause)
    {
        super(message,cause);
    }
}
