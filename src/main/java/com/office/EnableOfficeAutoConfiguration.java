package com.office;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 项目名： office-spring-boot-starter
 * 包路径： com.office
 * 作者：   TongHui
 * 创建时间: 2024-04-12 13:35
 * 描述: TODO
 * 版本: 1.1.0
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import(OfficeAutoConfiguration.class)
public @interface EnableOfficeAutoConfiguration {
}
