package com.office.core;

import java.util.Map;

/**
 * 处理wps回调消息
 */
public interface MessageHandle {

    /**
     * 企业已经打开文件总数，仅打开一个未打开的文件时调用，某些错误会导致不触发打开文件总数的回调通知
     * @param body 请求体
     */
    void OnlineFile(Map<String,Object> body);

    /**
     * 每次对文件进行打印或导出操作时调用
     * @param body 请求体
     */
    void OperateRecordExport(Map<String,Object> body);
    /**
     * 用户打开新页面时调用
     * @param body 请求体
     */
    void OpenPage(Map<String,Object> body);
    /**
     * 用户关闭页面时调用
     * @param body 请求体
     */
    void UserQuit(Map<String,Object> body);
    /**
     * 用户打开新页面时调用
     * @param body 请求体
     */
    void UserJoin(Map<String,Object> body);
    /**
     * 当文档从无变化到有变化的时候，会推送该消息
     * 文档状态改变时才会推送，如
     *      用户首次打开文档并编辑，会推送一次
     *      当用户保存文档成功后继续编辑才会推送（从保存状态变为改动状态）
     * @param body 请求体 可能为{}
     */
    void FileDirty(Map<String,Object> body);
    /**
     * 当文档从有改动状态变为已保存状态时，会推送该消息。通常为调用保存成功之后推送
     * @param body 请求体 可能为{}
     */
    void FileClear(Map<String,Object> body);
    /**
     * 当文档新增评论时，会推送该消息。
     * @param body 请求体
     */
    void CommentAdd(Map<String,Object> body);

    void ServerOpenFile(Map<String,Object> body);
}
