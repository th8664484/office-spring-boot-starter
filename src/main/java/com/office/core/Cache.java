package com.office.core;


public interface Cache {

     long TIMEOUT = 60000;
     long TIMEOUT_DAY = 86400000;

    String getCacheName();
    Object get(String key);
    void set(String key,Object value,long time);
    void set(String key,Object value);
    boolean hasKey(String key);
    void remove(String key);
}
