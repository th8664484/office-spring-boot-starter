package com.office.core;

import lombok.AllArgsConstructor;
import lombok.Data;

/*
 * @BelongsProject: office
 * @BelongsPackage: com.wps.core
 * @Author: TongHui
 * @CreateTime: 2024-03-15 11:36
 * @Description: TODO
 * @Version: 1.0
 */
@Data
public class CommonConfig {

    private String dowloadFile;

    private String localhostAddress;

    private Integer histNum;

    private Integer timeout;

    private Long maxSize = 20971520L;

    public CommonConfig(String dowloadFile, String localhostAddress, Integer histNum, Integer timeout, Long maxSize) {
        this.dowloadFile = dowloadFile;
        this.localhostAddress = localhostAddress;
        this.histNum = histNum;
        this.timeout = timeout;
        this.setMaxSize(maxSize);
    }

    public void setMaxSize(Long maxSize) {
        if (null != maxSize && maxSize > 0){
            this.maxSize = maxSize*1024*1024;
        }
    }
}
