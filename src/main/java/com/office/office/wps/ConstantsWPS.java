package com.office.office.wps;


/**
 * 自用常量集中管理
 */
public class ConstantsWPS {
    /**
     * 接口地址
     */
    // 请求编辑
    public static final String API_EDIT = "/api/edit/v1/files/${file_id}/link";
    //请求预览
    public static final String API_PREVIEW = "/api/preview/v1/files/${file_id}/link";
    //请求预览预处理
    public static final String API_PREVIEW_P = "/api/preview/v2/files/${file_id}/versions/{version_id}/preload";
    //文件转换 同步
    public static final String API_CONVERT_SYNC = "/api/cps/sync/v1/convert";
    //文件转换 异步
    public static final String API_CONVERT_ASYNC = "/api/cps/async/v1/convert";

    //文件下载
    public static final String API_DOWNLOAD = "/api/cps/v1/download/";

    // 任务查询
    public static final String API_TASK_QUERY_ASYNC = "/api/cps/async/v1/tasks/";

    //查询字体信息
    public static final String API_FONTS_QUERY = "/api/cps/async/v1/query/fonts";
    //查询是否为限制编辑状态
    public static final String API_QUERYEDITING_ASYNC = "/api/cps/async/v1/query/editing";
    public static final String API_QUERYEDITING_SYNC = "/api/cps/sync/v1/query/editing";
    //开启/禁止文档编辑
    public static final String API_DOCEDITING_ASYNC = "/api/cps/async/v1/document/editing";
    public static final String API_DOCEDITING_SYNC = "/api/cps/sync/v1/document/editing";
    //在线解压
    public static final String API_ONLINEDECOM_ASYNC = "/api/cps/async/v1/decompression";
    public static final String API_ONLINEDECOM_SYNC = "/api/cps/sync/v1/decompression";
    //图片操作
    public static final String API_IMAGE_ASYNC = "/api/cps/async/v1/image/operate";
    public static final String API_IMAGE_SYNC = "/api/cps/sync/v1/image/operate";
    //文档密码修改
    public static final String API_PASSWORD_ASYNC = "/api/cps/async/v1/password";
    public static final String API_PASSWORD_SYNC = "/api/cps/sync/v1/password";
    //查询只读/可编辑书签
    public static final String API_BOOKMARK_ASYNC = "/api/cps/async/v1/query/bookmarkpermissions";
    public static final String API_BOOKMARK_SYNC = "/api/cps/sync/v1/query/bookmarkpermissions";
    //限制书签
    public static final String API_RESTRICTBOOKMARK_ASYNC = "/api/cps/async/v1/set/bookmarkpermissions";
    public static final String API_RESTRICTBOOKMARK_SYNC = "/api/cps/sync/v1/set/bookmarkpermissions";
    //查询书签
    public static final String API_QUERYBOOKMARK_ASYNC = "/api/cps/async/v1/query/bookmark";
    public static final String API_QUERYBOOKMARK_SYNC = "/api/cps/sync/v1/query/bookmark";
    //文档拆分
    public static final String API_SPLIT_ASYNC = "/api/cps/async/v1/split";
    public static final String API_SPLIT_SYNC = "/api/cps/sync/v1/split";
    //文档合并
    public static final String API_MERGE_ASYNC = "/api/cps/async/v1/merge";
    public static final String API_MERGE_SYNC = "/api/cps/sync/v1/merge";
    //内容操作
    public static final String API_OPERATE_ASYNC = "/api/cps/async/v1/content/operate";
    public static final String API_OPERATE_SYNC = "/api/cps/sync/v1/content/operated";
    //多书签套用
    public static final String API_WRAPHEADER_ASYNC = "/api/cps/async/v1/wrapheader";
    public static final String API_WRAPHEADER_SYNC = "/api/cps/sync/v1/wrapheader";

    /**
     * 自定义参数前缀
     */
    public static final String PREFIX = "_w_third_";

    /**
     * 请求头
     */
    public static final String CONTENT_TYPE = "application/json";
    /**
     * 是否传输token
     */
    public static final int W_TOKENTYPE_YES = 1;
    public static final int W_TOKENTYPE_NO  = 0;


    /**
     * 以用的自定义参数
     */

    public static final String APPID = "appid";

    public static final String FILEID = "file_id";

    /**
     * 回调类型
     */
    public static final String CMD_ONLINE = "OnlineFileCountCmd";
    public static final String CMD_EXPORT = "OperateRecordExport";
    public static final String CMD_OPENPAGE = "OpenPageCmd";
    public static final String CMD_QUIT = "UserQuit";
    public static final String CMD_JOIN = "UserJoin";
    public static final String CMD_DIRTY = "FileDirty";
    public static final String CMD_CLEAR = "FileClear";
    public static final String CMD_COMMENT = "CommentAdd";
    public static final String CMD_SERVEROPENFILE = "ServerOpenFile";

    /**
     * 用户权限  1：打开 0：关闭
     */
    public static final Integer USER_RENAME_N = 0;        //重命名 默认0
    public static final Integer USER_RENAME_y = 1;        //重命名 默认0
    public static final Integer USER_HISTORY_N = 0;       //历史文件打开 默认1
    public static final Integer USER_HISTORY_Y = 1;       //历史文件打开 默认1
    public static final Integer USER_COPY_N = 0;          //复制 默认1
    public static final Integer USER_COPY_Y = 1;          //复制 默认1
    public static final Integer USER_EXPORT_N = 0;        //导出 默认1
    public static final Integer USER_EXPORT_Y = 1;        //导出 默认1
    public static final Integer USER_PRINT_N = 0;         //打印 默认1
    public static final Integer USER_PRINT_Y = 1;         //打印 默认1
    public static final Integer USER_COMMENT_N = 0;       //只读情况下的可评论 默认0
    public static final Integer USER_COMMENT_Y = 1;       //只读情况下的可评论 默认0
    public static final Integer USER_COPOY_CONTROL_N = 0; //限制在当前系统内复制粘贴 默认0
    public static final Integer USER_COPOY_CONTROL_Y = 1; //限制在当前系统内复制粘贴 默认0

    /**
     * 水印
     */
    public static final Integer WATERMARK_TYPE_Y = 1; //水印类型， 0为无水印； 1为文字水印
    public static final Integer WATERMARK_TYPE_N = 0;
}
