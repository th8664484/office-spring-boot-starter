package com.office.office.wps.dto;


import lombok.Data;

import java.util.Map;

@Data
public class OnnotifyMessage {
    String cmd;

    Map<String,Object> body;
}
