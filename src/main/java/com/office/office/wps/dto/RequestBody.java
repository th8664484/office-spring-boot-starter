package com.office.office.wps.dto;

import lombok.Data;

/**
 * 项目名： office-spring-boot-starter
 * 包路径： com.office.office.wps.dto
 * 作者：   TongHui
 * 创建时间: 2024-03-27 11:15
 * 描述: 查询字体信息
 * 版本: 1.0
 */
@Data
public class RequestBody {
    String task_id;
    String doc_filename;
    String doc_url;
}
