package com.office.office.wps.dto;

import lombok.Data;

/**
 * 文件转换结果
 */
@Data
public class FileConvertResult {
    /**
     * 下载id
     */
    String download_id;
    /**
     * 请求 下载接口 时需放在header中
     */
    String route_key;
}
