package com.office.office.wps.dto;

import com.office.tools.wps.TaskStatus;
import com.office.tools.wps.TaskType;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * 任务查询结果
 */
@Data
public class TaskQueryResult {
    /**
     * 文件id,可用于下载
     */
    String download_id;
    /**
     * 任务执行失败时的详细描述
     */
    String message;
    /**
     * 任务类型 ,枚举类型
     */
    TaskType method;
    /**
     * 任务状态
     */
    TaskStatus status;

    /**
     * 返回的自定义参数列表
     */
    Map<String,Object> query_params;

    /**
     * 压缩包中的文件列表
     */
    List<FileDowlaodInfo>  decompression_file_list;
}
