package com.office.office.wps.dto;

import lombok.Data;

import java.util.List;

/**
 * 文件下载信息
 */
@Data
public class FileDowlaodInfo {
    /**
     * 下载id，directory为false时返回
     */
    String download_id;
    String path;
    /**
     * 文件/文件夹名称
     */
    String name;
    /**
     * 文件大小
     */
    String size;
    /**
     * 是否文件夹
     */
    Boolean directory;
    /**
     * 是否加密
     */
    Boolean encrypted;
    /**
     * 最后修改时间
     */
    Long modify_time;

    /**
     * 文件夹中的文件列表，directory为true时存在
     */
    List<FileDowlaodInfo> list_files;
}
