package com.office.config.oo;

import lombok.Data;

/**
 * 配置信息
 */
@Data
public class OnlyProperties {
    /**
     * 回调地址 不包含http表示本机地址
     */
    private String callBackUrl ;

    /**
     * only office服务路径
     * 必须存在
     */
    private String docService ;

    /**
     * JWT令牌
     */
    private String secret ;


    public final String DOC_API_URL = "/web-apps/apps/api/documents/api.js";
    public final String CONVERTER = "/ConvertService.ashx";
    public final String SAVE = "/coauthoring/CommandService.ashx";


}
