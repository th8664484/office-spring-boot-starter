package com.office.config.wps;

/*
 * @BelongsProject: office
 * @BelongsPackage: com.office.config.wps
 * @Author: TongHui
 * @CreateTime: 2024-03-13 11:21
 * @Description: TODO
 * @Version: 1.0
 */
public class FileInfo {

    //文件id,字符串长度不超过64位
    private String id;
    //文件名必须带后缀
    private String name;
    //修改者id，字符串长度不超过32位
    private String modifier;
    //文档下载地址
    private String download_url;

    //文档版本号，从1开始累加，位数小于11
    private Integer version;
    //文档大小，单位为字节；
    private Long size;
    //创建者id，字符串长度不超过32位
    private String creator;
    //创建时间，时间戳，单位为秒
    private Long create_time;
    //最近修改时间，时间戳，单位为秒
    private Long modify_time;
    /**
     * 限制预览页数（不超过5000）
     * 1. 用户操作权限 user.permission 为 write 时，限制
     * 不生效；
     * 2. 用户操作权限 user.permission 为 read 时，
     * previewPages 默认值为 0，不限制预览页数；
     * previewPages >= 1 时，限制生效，限制的页数为
     * previewPages 字段的值
     */
    private Integer preview_pages;

    private UserAcl user_acl;
    private FileWatermark watermark;

    public FileInfo(String id, String name, String modifier, String download_url, Integer version, Long size, String creator, Long create_time, Long modify_time) {
        this.id = id;
        this.name = name;
        this.modifier = modifier;
        this.download_url = download_url;
        this.version = version;
        this.size = size;
        this.creator = creator;
        this.create_time = create_time;
        this.modify_time = modify_time;
    }

    public FileInfo(String id, String name, String modifier, String download_url, Integer version, Long size, String creator, Long create_time, Long modify_time, Integer preview_pages, UserAcl user_acl, FileWatermark watermark) {
        this.id = id;
        this.name = name;
        this.modifier = modifier;
        this.download_url = download_url;
        this.version = version;
        this.size = size;
        this.creator = creator;
        this.create_time = create_time;
        this.modify_time = modify_time;
        this.preview_pages = preview_pages;
        this.user_acl = user_acl;
        this.watermark = watermark;
    }

    public FileInfo() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getDownload_url() {
        return download_url;
    }

    public void setDownload_url(String download_url) {
        this.download_url = download_url;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        if (version <= 0){
            version = 1;
        }
        this.version = version;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Long getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Long create_time) {
        this.create_time = create_time;
    }

    public Long getModify_time() {
        return modify_time;
    }

    public void setModify_time(Long modify_time) {
        this.modify_time = modify_time;
    }

    public Integer getPreview_pages() {
        return preview_pages;
    }

    public void setPreview_pages(Integer preview_pages) {
        this.preview_pages = preview_pages;
    }

    public UserAcl getUser_acl() {
        return user_acl;
    }

    public void setUser_acl(UserAcl user_acl) {
        this.user_acl = user_acl;
    }

    public FileWatermark getWatermark() {
        return watermark;
    }

    public void setWatermark(FileWatermark watermark) {
        this.watermark = watermark;
    }
}
