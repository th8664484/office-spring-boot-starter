package com.office.config.wps;

import lombok.Data;

/*
 * @BelongsProject: office-spring-boot-starter
 * @BelongsPackage: com.office.config.wps
 * @Author: TongHui
 * @CreateTime: 2024-03-18 15:47
 * @Description: TODO
 * @Version: 1.0
 */
@Data
public class ToPng {

    /**
     * 仅在文字、演示、PDF格式转png时生效。是否转换为
     * 长图。默认为 false，按页导出图片。当 long_pic 为
     * true 时，必须满足 to_page=-1 或者 from_page 和
     * to_page 同时非空。备注：转长图目前最大可支持转20
     * 页空白文档，在这个基础上，文档的内容越多，转换成
     * 功的范围就越小（指最终文件会出现空白页）。建议范
     * 围不超过12页。
     */
    Boolean  long_pic;
    Integer from_page;
    Integer to_page;
    Integer sheet_index;

}
