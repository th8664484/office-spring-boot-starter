package com.office.config.wps;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * ofd stamp水印参数。仅老内核ofd转pdf有效（新内核
 * 直接转换文档，没有此参数）
 */
@Data
public class Ofdseal {
    String type;
    String content;
    Map<String,Object> font;
    Integer delta_x;
    Integer delta_y;

    public void setFont(String fontName,Integer fontSize,Boolean bold,Boolean italic,String color) {
        this.font = new HashMap<String, Object>();
        this.font.put("font_name",fontName);
        this.font.put("font_size",fontSize);
        this.font.put("bold",bold);
        this.font.put("italic",italic);
        this.font.put("color",color);
    }
}
