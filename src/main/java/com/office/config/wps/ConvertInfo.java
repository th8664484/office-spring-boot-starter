package com.office.config.wps;

import com.office.tools.wps.HMacUtils;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/*
 * @BelongsProject: office
 * @BelongsPackage: com.office.config.wps
 * @Author: TongHui
 * @CreateTime: 2024-03-13 16:58
 * @Description: TODO
 * @Version: 1.0
 */
@Data
public class ConvertInfo {

    private String task_id;
    private String doc_url;
    private String doc_filename;
    private String doc_password;
    private String target_file_format;

    //word转html格式时的文档编码。10008为GB2312，65001为UTF-8
    private String web_encoding;

    private EtPageZoom et_page_zoom;
    private Ofdseal ofdseal;


    private ToPng to_png;

    /**
     * 转换为pdf的参数
     *
     */
    private ToPdf to_pdf;

    // dpi 仅在pdf转图片时生效。设置转换结果图片的dpi
    private Map<String,Object> to_img;

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public void setDoc_url(String doc_url) {
        this.doc_url = doc_url;
    }

    public void setDoc_filename(String doc_filename) {
        this.doc_filename = doc_filename;
    }

    public void setDoc_password(String doc_password) {
        this.doc_password = doc_password;
    }

    public void setTarget_file_format(String target_file_format) {
        this.target_file_format = target_file_format;
    }

    public void setWeb_encoding(String web_encoding) {
        this.web_encoding = web_encoding;
    }

    public void setEt_page_zoom(EtPageZoom et_page_zoom) {
        this.et_page_zoom = et_page_zoom;
    }

    public void setOfdseal(Ofdseal ofdseal) {
        this.ofdseal = ofdseal;
    }

    public void setTo_png(ToPng to_png) {
        this.to_png = to_png;
    }

    public void setTo_pdf(ToPdf to_pdf) {
        this.to_pdf = to_pdf;
    }

    public void setTo_img(Map<String, Object> to_img) {
        this.to_img = to_img;
    }

    public static class Builder{
        private ConvertInfo convertInfo;

        public Builder(){
            this.convertInfo = new ConvertInfo();
        }
        public ConvertInfo builder(){
            return this.convertInfo;
        }

        public Builder taskId(String taskId){
            this.convertInfo.setTask_id(taskId);
            return this;
        }
        public Builder docUrl(String docUrl){
            this.convertInfo.setDoc_url(docUrl);
            return this;
        }
        public Builder docFilename(String docFilename){
            this.convertInfo.setDoc_filename(docFilename);
            return this;
        }
        public Builder docPassword(String key,String docPassword) throws Exception {
            if (docPassword != null && !docPassword.isEmpty()){
                String aes_ecb_pkcs5PADDING = HMacUtils.getAES_ECB_PKCS5PADDING(key, docPassword);
                this.convertInfo.setDoc_password(aes_ecb_pkcs5PADDING);
            }
            return this;
        }
        public Builder targetFileFormat(String targetFileFormat){
            this.convertInfo.setTarget_file_format(targetFileFormat);
            return this;
        }
        public Builder webEncoding(String webEncoding){
            this.convertInfo.setWeb_encoding(webEncoding);
            return this;
        }
        public Builder etPageZoom(EtPageZoom etPageZoom){
            this.convertInfo.setEt_page_zoom(etPageZoom);
            return this;
        }

        public Builder ofdseal(Ofdseal ofdseal){
            this.convertInfo.setOfdseal(ofdseal);
            return this;
        }

        public Builder toPng(ToPng toPng){
            this.convertInfo.setTo_png(toPng);
            return this;
        }

        public Builder toPdf(ToPdf toPdf){
            this.convertInfo.setTo_pdf(toPdf);
            return this;
        }

        public Builder toImg(Integer dpi){
            Map<String,Object> img = new HashMap<String,Object>();
            img.put("dpi",dpi);
            this.convertInfo.setTo_img(img);
            return this;
        }
    }
}
