package com.office.config.wps;

import lombok.Data;

/*
 * @BelongsProject: office
 * @BelongsPackage: com.wps.config.wps
 * @Author: TongHui
 * @CreateTime: 2024-03-15 14:53
 * @Description: TODO
 * @Version: 1.0
 */
@Data
public class WPSBaseInfo {

    private String ak;

    private String sk;

    private String domainName;

    private String secretKey;
}
