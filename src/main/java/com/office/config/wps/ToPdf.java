package com.office.config.wps;

import lombok.Data;

/*
 * @BelongsProject: office-spring-boot-starter
 * @BelongsPackage: com.office.config.wps
 * @Author: TongHui
 * @CreateTime: 2024-03-18 15:47
 * @Description: TODO
 * @Version: 1.0
 */
@Data
public class ToPdf {

    //仅在文字转pdf时生效。设置文档中的图片转为pdf时的质量
    Integer jpeg_quality;
    //仅在文字、演示转pdf时生效。转换起始页，从 1 开始计数。文字、演示、PDF格式转换成PDF和PNG时，能控制输出范围。
    Integer from_page;
    //仅在文字、演示转pdf时生效。转换结束页，to_page需要大于 from_page，to_page也可以指定为 -1，导出到最后一页（全部转换）。
    Integer to_page;
    //仅在表格 Excel 转换pdf时生效。指定需要转换的Sheet，从 1 开始计数。不传，表示转换所有 Sheet
    Integer sheet_index;
    //仅在表格 Excel 转换pdf时生效。纸张方向，选项: 2 -> 纸张横放; 1 -> 纸张竖放; 输入其他默认纸张竖放
    Integer  orientation;
    //仅在图片转pdf时生效)指定页面大小：0: A4, 3: A5,传无效参数，默认A4
    Integer  paper_size;
    //仅在图片转pdf时生效)当使用paper_size指定页面大小时根据图片尺寸，自动使用横向或纵向的纸张，默认true
    Boolean  auto_rotate_paper;
    //仅在图片转pdf时生效)当使用paper_size指定页面大小时:true: 小图片放大 到页面，保持纵横比；false: 小图片居中
    Boolean  scale_small_image;
}
