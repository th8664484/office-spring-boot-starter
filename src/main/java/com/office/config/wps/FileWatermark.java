package com.office.config.wps;

import lombok.Data;

/*
 * @BelongsProject: office
 * @BelongsPackage: com.office.config.wps
 * @Author: TongHui
 * @CreateTime: 2024-03-13 11:26
 * @Description: TODO
 * @Version: 1.0
 */
@Data
public class FileWatermark {

    //水印类型， 0为无水印； 1为文字水印
    private Integer type ;
    //水印水平间距，非必选，有默认值，默认为50
    private Integer horizontal ;
    //水印垂直间距，非必选，有默认值，默认为100
    private Integer vertical ;
    //文字水印的文字，支持通过 \r\n 换行，支持emoji表情
    //当type为1时此字段必选
    private String value ;
    //水印的颜色（含透明度），非必选，有默认值。格式为： rgba( 192, 192, 192, 0.6 )
    private String fillstyle ;
    //水印的字体，非必选，有默认值，格式为 bold 20px Serif
    private String font ;
    //水印的旋转度(弧度)，非必选，有默认值，默认为-0.7853982
    private Float rotate ;
}
