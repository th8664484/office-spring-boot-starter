package com.office.config.wps;

/*
 * @BelongsProject: office
 * @BelongsPackage: com.office.config.wps
 * @Author: TongHui
 * @CreateTime: 2024-03-13 11:22
 * @Description: TODO
 * @Version: 1.0
 */
public class UserAcl {
    //重命名权限，1为打开该权限，0为关闭该权限，默认为0
    private Integer rename = 0 ;
    //历史文件打开权限，1为打开该权限，0为关闭该权限，默认为1
    private Integer history = 1 ;
    //复制权限，1为打开该权限，0为关闭该权限，默认为1
    private Integer copy = 1 ;
    //导出权限，1为打开该权限，0为关闭该权限，默认为1
    private Integer export = 1;
    //打印权限，1为打开该权限，0为关闭该权限，默认为1
    private Integer print = 1 ;
    //只读情况下的可评论权限，1为打开该权限，0为关闭该权限，默认为0
    private Integer comment = 0 ;
    //限制在当前系统内复制粘贴，1为打开该权限，0为关闭该权限，默认为0
    private Integer copy_control = 0 ;

    public UserAcl(Integer rename, Integer history, Integer copy, Integer export, Integer print, Integer comment, Integer copy_control) {
        this.rename = rename;
        this.history = history;
        this.copy = copy;
        this.export = export;
        this.print = print;
        this.comment = comment;
        this.copy_control = copy_control;
    }

    public UserAcl() {
    }

    public Integer getRename() {
        return rename;
    }

    public void setRename(Integer rename) {
        this.rename = rename;
    }

    public Integer getHistory() {
        return history;
    }

    public void setHistory(Integer history) {
        this.history = history;
    }

    public Integer getCopy() {
        return copy;
    }

    public void setCopy(Integer copy) {
        this.copy = copy;
    }

    public Integer getExport() {
        return export;
    }

    public void setExport(Integer export) {
        this.export = export;
    }

    public Integer getPrint() {
        return print;
    }

    public void setPrint(Integer print) {
        this.print = print;
    }

    public Integer getComment() {
        return comment;
    }

    public void setComment(Integer comment) {
        this.comment = comment;
    }

    public Integer getCopy_control() {
        return copy_control;
    }

    public void setCopy_control(Integer copy_control) {
        this.copy_control = copy_control;
    }
}
