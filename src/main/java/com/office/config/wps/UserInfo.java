package com.office.config.wps;

import com.office.tools.wps.UserPermission;

/*
 * @BelongsProject: office
 * @BelongsPackage: com.office.config.wps
 * @Author: TongHui
 * @CreateTime: 2024-03-13 11:21
 * @Description: TODO
 * @Version: 1.0
 */
public class UserInfo {

    //用户id，长度不超过32位
    private String id;
    //用户名称
    private String name;
    //用户操作权限，write：可编辑，read：只读模式
    private UserPermission permission;
    //用户头像地址，支持url和base64
    private String avatar_url;

    private UserInfo() {
    }

    public UserInfo(String id, String name, UserPermission permission) {
        this.id = id;
        this.name = name;
        this.permission = permission;
    }

    public UserInfo(String id, String name, UserPermission permission, String avatar_url) {
        this.id = id;
        this.name = name;
        this.permission = permission;
        this.avatar_url = avatar_url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserPermission getPermission() {
        return permission;
    }

    public void setPermission(UserPermission permission) {
        this.permission = permission;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }
}
