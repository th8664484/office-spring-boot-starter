package com.office.config.wps;

import lombok.Data;

/*
 * @BelongsProject: office-spring-boot-starter
 * @BelongsPackage: com.office.config.wps
 * @Author: TongHui
 * @CreateTime: 2024-03-18 15:46
 * @Description: //表格转换参数
 * @Version: 1.0
 */
@Data
public class EtPageZoom {
    /**
     * 表示是否保持当前客户端的缩放比，true表示保持当前
     * 缩放比打印，false表示以100%的缩放比打印，当
     * fit_pagetall或fit_pagewide中有一个为1，或都为1时，
     * 该参数不生效(新内核不支持)
     */
    Boolean keep_pagezoom;
    /**
     * 表示是否适配所有行，0表示正常分页打印，1表示不分
     * 页，所有行在一页上
     */
    Integer fit_pagewide;
    /**
     * 表示是否适配所有列，0表示正常分页打印，1表示不分
     * 页，所有列在一页上；当fit_pagetall与fit_pagewide都
     * 为1时，表示将所有内容打印到一页上
     */
    Integer fit_pagetall;
}
