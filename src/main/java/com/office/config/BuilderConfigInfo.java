package com.office.config;


import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import com.office.config.oo.*;
import com.office.config.oo.document.DocumentInfo;
import com.office.config.oo.document.DocumentPermission;
import com.office.config.oo.document.SharingSettings;
import com.office.config.oo.edit.FileCustomization;
import com.office.config.oo.edit.FileUser;
import com.office.config.wps.FileInfo;
import com.office.config.wps.FileWatermark;
import com.office.config.wps.UserAcl;
import com.office.config.wps.UserInfo;
import com.office.exception.OfficeException;
import com.office.tools.FileUtil;
import com.office.tools.oo.JWTUtil;
import com.office.tools.oo.OnlyOfficeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class BuilderConfigInfo {
    protected final static Logger logger = LoggerFactory.getLogger(BuilderConfigInfo.class);

    /*
     *  初始化 wps office 最基础的信息必要数据
     *                  必填
     * @param fileInfo  是
     * @param userAcl   否
     * @param watermark 否
     * @param userInfo  是
     * @return
     */
    public static Map<String,Object> builderWpsConfig(FileInfo fileInfo, UserAcl userAcl, FileWatermark watermark, UserInfo userInfo){
        if (null == fileInfo || null == userInfo){
            throw new OfficeException("缺少必填参数");
        }
        fileInfo.setUser_acl(userAcl);
        fileInfo.setWatermark(watermark);

        Map<String,Object> config = new HashMap<>();
        config.put("file",fileInfo);
        config.put("user",userInfo);
        String json = JSONUtil.toJsonStr(config);
        config = JSONUtil.toBean(json, Map.class);
        return config;
    }

    /*
     * 初始化 only office 最基础的信息必要数据
     *
     * @param fileUrl  可访问的url路径
     * @param mode     打开方式
     * @param key      唯一标示符 20个字符以内
     * @param fileName 文件名称
     * @return 配置信息
     */

    public static FileConfig buildOnlyConfig(String fileUrl, String mode, String key, String fileName, String callBackUrl,
                                             OnlyProperties onlyProperties, FileUser user,
                                             Plugins plugins, DocumentPermission permission, FileCustomization customPermissions){
        Map<String, Object> map = new HashMap<>();

        FileConfig fileConfigDTO = new FileConfig();
        // 1、文档类型
        fileConfigDTO.setDocumentType(OnlyOfficeUtil.getDocumentType(fileName));
        map.put("documentType", fileConfigDTO.getDocumentType());
        map.put("type", fileConfigDTO.getType());
        // 2、onlyoffice的 api位置
        fileConfigDTO.setDocServiceApiUrl(onlyProperties.getDocService() + onlyProperties.getDOC_API_URL());

        // ========文档类型=============
        String typePart = FileUtil.getFileExtension(fileName);
        DocumentConfig fileDocument = DocumentConfig.builder()
                // 文件名
                .title(fileName)
                // 扩展名
                .fileType(typePart)
                // 可访问的url
                .url(fileUrl)
                // 唯一有标示符
                .key(key)
                .info(getDocumentInfo(user))
                .permissions(permission)
                .build();
        fileConfigDTO.setDocument(fileDocument);
        map.put("document", fileDocument);
        // ==========编辑配置===============

        EditorConfig editorConfig = new EditorConfig(callBackUrl, mode);
        editorConfig.setFileCustomization(customPermissions);
        editorConfig.setFileUser(user);
        editorConfig.setPlugins(plugins);


        fileConfigDTO.setEditorConfig(editorConfig);
        map.put("editorConfig", editorConfig);


        if (null != onlyProperties.getSecret()){
            String token =  JWTUtil.createToken(map,onlyProperties.getSecret());
            fileConfigDTO.setToken(token);
        }

        return fileConfigDTO;
    }

    private static DocumentInfo getDocumentInfo(FileUser user) {
        SharingSettings sharingSettings = new SharingSettings();
        sharingSettings.setPermissions(new String[]{"Full Access"});
        sharingSettings.setUser(user.getName());
        sharingSettings.setIsLink(true);
        DocumentInfo info = DocumentInfo.builder()
                .created(DateUtil.formatDateTime(new Date()))
                .sharingSettings(Collections.singletonList(sharingSettings)).build();
        return info;
    }

}
