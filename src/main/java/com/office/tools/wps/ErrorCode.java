package com.office.tools.wps;

/**
 * 回调错误
 */
public enum ErrorCode {


    SessionExpired(40002,"token过期"),
    PermissionDenied(40003,"用户无权限访问"),
    NotExists(40004,"资源不存在"),
    InvalidArgument(40005,"参数错误"),
    SpaceFull(40006,"保存空间已满"),
    CustomMsg(40007,""),
    FnameConflict(40008,"文件重命名冲突"),
    ServerError(50001,"对接系统错误"),
    CodeParamFormatError(40000002,"请求参数格式有误"),
    CodeResourceNotFound(40000103,"服务注册校验，服务未注册或查看router_register_info表是否"),
    CodeRemoteFai(40000201,"远程调用失败（客户端错误）"),
    CodeAuthError(40100001,"认证错误"),
    Code400Begin(40010001,"管理后台业务接口异常"),
    CodeRemoteError(50000001,"转发请求的服务无响应或无效的IP访问"),
    ;
    Integer code;
    String value;
    ErrorCode(Integer code, String value) {
        this.code = code;
        this.value = value;
    }

    public Integer getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public static String getValueByCode(Integer code) {
        for (ErrorCode errorCode : ErrorCode.values()) {
            if (errorCode.code.equals(code)) {
                return errorCode.value;
            }
        }
        return "";
    }


}
