package com.office.tools.wps;


import com.office.tools.FileUtil;

import java.util.Arrays;
import java.util.List;

/*
 * @BelongsProject: office
 * @BelongsPackage: com.office.tools.wps
 * @Author: TongHui
 * @CreateTime: 2024-03-12 18:26
 * @Description: 文件类型
 * @Version: 1.0
 */
public class WPSFileTypeUtil {

    public static String openEdit(String fileName){
        // 获取文件的扩展名
        String ext = FileUtil.getFileExtension(fileName);
        if (ExtsDocument.contains(ext)) {
            return FileType.W.toString().toLowerCase();
        } else if (ExtsSpreadsheet.contains(ext)) {
            return FileType.S.toString().toLowerCase();
        } else if (ExtsPresentation.contains(ext)) {
            return FileType.P.toString().toLowerCase();
        }else if (PDF.equals(ext)){
            return FileType.F.toString().toLowerCase();
        }

        throw new IllegalArgumentException("【"+ext+"】无效文件类型");
    }



    private static List<String> ExtsDocument = Arrays.asList(
            "doc", "docx", "docm",
            "dot", "dotx", "dotm",
            "wps", "wpt");

    private static List<String> ExtsSpreadsheet = Arrays.asList(
            "xls", "xlsx", "xlsm",
            "xlt", "xltx", "xltm",
            "et");

    private static List<String> ExtsPresentation = Arrays.asList(
            "pps", "ppsx", "ppsm",
            "ppt", "pptx", "pptm",
            "dpt", "potx", "potm",
            "dps");

    public static String openPreview(String fileName){
        // 获取文件的扩展名
        String ext = FileUtil.getFileExtension(fileName);
        if (previewDocument.contains(ext)) {
            return FileType.W.toString().toLowerCase();
        } else if (previewSpreadsheet.contains(ext)) {
            return FileType.S.toString().toLowerCase();
        } else if (previewPresentation.contains(ext)) {
            return FileType.P.toString().toLowerCase();
        }else if (PDF.equals(ext) || OFD.equals(ext)) {
            return FileType.F.toString().toLowerCase();
        }else if (previewX.contains(ext)){
            return FileType.X.toString().toLowerCase();
        }

        throw new IllegalArgumentException("【"+ext+"】无效文件类型");
    }

    private static List<String> previewDocument = Arrays.asList(
            "doc", "docx", "docm",
            "dot", "dotx", "dotm",
            "wps", "wpt","rtf" ,
            "txt" ,"mht" , "mhtml" , "htm" ,
            "html" , "uot3");

    private static List<String> previewSpreadsheet = Arrays.asList(
            "xls", "xlsx", "xlsm",
            "xlt", "xltx", "xltm",
            "et","ett","csv");

    private static List<String> previewPresentation = Arrays.asList(
            "pps", "ppsx", "ppsm",
            "ppt", "pptx", "pptm",
            "dpt", "potx", "potm",
            "dps");
    private static List<String> previewX = Arrays.asList(
            "jpeg" , "jpg" , "png" , "gif" , "bmp" ,"tif" , "tiff" , "svg" , "psd",
            "tar" , "zip" , "7z" , "jar" , "rar" , "gzip", "md",
            "c" , "cpp" , "java" , "js" , "css" ,
            "lrc" , "h" , "asm" , "s" , "asp" ,
            "bat" , "bas" , "prg" , "cmd" , "xml",
            "log" , "ini" , "inf",
            "cdr" , "vsd" , "vsdx"
            );

    private static String PDF = "pdf";
    private static String OFD = "ofd";


    enum FileType {
        W,S,P,F,X
    }
}
