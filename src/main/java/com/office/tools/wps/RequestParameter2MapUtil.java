package com.office.tools.wps;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/*
 * @BelongsProject: office
 * @BelongsPackage: com.office.tools.wps
 * @Author: TongHui
 * @CreateTime: 2024-03-15 09:23
 * @Description: 获取自定义参数
 * @Version: 1.0
 */
public class RequestParameter2MapUtil {

    private static final String PREFIX = "_w_third_";

    /*
     * 变量所有参数找到指定字符开头的参数，并返回
     * @param request
     * @return
     */
    public static Map<String,Object> requestParam2Map(HttpServletRequest request){
        Map<String, Object> paramMap = new HashMap<>();

        // 获取GET请求参数
        Map<String, String[]> parameterMap = request.getParameterMap();
        for (String paramName : parameterMap.keySet()) {
            if (paramName.startsWith(PREFIX)){
                String[] paramValues = parameterMap.get(paramName);
                if (paramValues.length > 0) {
                    paramMap.put(paramName.replace(PREFIX,""), paramValues[0]);
                }
            }
        }
        return paramMap;
    }
}
