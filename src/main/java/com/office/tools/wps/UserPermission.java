package com.office.tools.wps;

/**
 * 项目名： office-spring-boot-starter
 * 包路径： com.office.tools.wps
 * 作者：   TongHui
 * 创建时间: 2024-03-27 10:24
 * 描述: 用户权限
 * 版本: 1.0
 */
public enum UserPermission {
    write,
    read;
}
