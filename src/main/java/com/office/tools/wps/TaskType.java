package com.office.tools.wps;

/**
 * 任务类型
 */
public enum TaskType {
    //文档转换
    OFFICE_CONVERT,
    //多书签模板套用
    OFFICE_WARP_HEADER,
    //文档拆分
    OFFICE_MERGE,
    //智能公文
    OFFICE_SMART_OFFICIAL,
    // 内容操作
    OFFICE_OPERATE,
    //查询文档书签
    OFFICE_QUERY_BOOKMARK,
    //限制书签编
    OFFICE_SET_BOOKMARK_PERMS,
    //查询只读/可编辑
    OFFICE_QUERY_BOOKMARK_PERMS,
    //图片操作
    IMAGE_OPERATE,
    //文档密码修改
    OFFICE_PASSWORD,
    //文档转换pdf转docx
    PDF_CONVERTOR;
}
