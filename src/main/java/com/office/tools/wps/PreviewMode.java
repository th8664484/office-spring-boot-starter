package com.office.tools.wps;


/**
 * 预览模式，可选值：
 * high_definition :高清预览
 * ordinary :普通预览
 * cache :缓存预览
 * official :公文极速预览 不传默认official
 */
public enum PreviewMode {

    high_definition,
    ordinary,
    cache,
    official;
}
