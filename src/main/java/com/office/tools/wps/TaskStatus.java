package com.office.tools.wps;

/**
 * 任务状态
 */
public enum TaskStatus {
    WAITING ,
    SUCCESS ,
    FAIL;
}
