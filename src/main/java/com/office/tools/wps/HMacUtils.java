package com.office.tools.wps;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/*
 * @BelongsProject: office
 * @BelongsPackage: com.office.tools.wps
 * @Author: TongHui
 * @CreateTime: 2024-03-12 19:01
 * @Description: TODO
 * @Version: 1.0
 */
public class HMacUtils {

    public static String HMACSHA256(String data, String key) throws Exception {
        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
        SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
        sha256_HMAC.init(secret_key);
        byte[] array = sha256_HMAC.doFinal(data.getBytes("UTF-8"));
        StringBuilder sb = new StringBuilder();
        for (byte item : array) {
            sb.append(Integer.toHexString((item & 0xFF) | 0x100).substring(1, 3));
        }
        return sb.toString();
    }


    /**
     * 利用java原生的摘要实现SHA256加密
     * @param str 要加密的内容
     * @return 加密后的报文
     */
    public static String getSHA256StrJava(byte[] str){
        MessageDigest messageDigest;
        String encodeStr = "";
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(str);
            encodeStr = byte2Hex(messageDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return encodeStr;
    }

    /**
     *
     * @param key    密钥
     * @param value  加密内容
     * @return 加密后的报文
     */
    public static String getAES_ECB_PKCS5PADDING(String key, String value) throws Exception {
        // 创建AES加密器
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(), "AES");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);

        // 加密
        byte[] encryptedBytes = cipher.doFinal(value.getBytes());
        String encryptedText = base64Encode(encryptedBytes);
//        System.out.println("Encrypted Text: " + encryptedText);
        return encryptedText;
    }

    /**
     * base64 编码
     * @param value 内容
     * @return 加密后的报文
     */
    public static String base64Encode(byte[] value){
        // 编码操作
        byte[] encodedBytes = Base64.getEncoder().encode(value);
        String encodedString = new String(encodedBytes);
        return encodedString;
    }
    /**
     * 将byte转为16进制
     * @param bytes 内容
     * @return 加密后的报文
     */
    private static String byte2Hex(byte[] bytes){
        StringBuffer stringBuffer = new StringBuffer();
        String temp = null;
        for (int i=0;i<bytes.length;i++){
            temp = Integer.toHexString(bytes[i] & 0xFF);
            if (temp.length()==1){
                //1得到一位的进行补0操作
                stringBuffer.append("0");
            }
            stringBuffer.append(temp);
        }
        return stringBuffer.toString();
    }

    public static void main(String[] args) throws Exception {
        String key = "XDWe0nNGxTg2yD8Gb3uUapkoA8XtKvq3"; // 16字节的密钥
        String plainText = "123";

        // 创建AES加密器
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(), "AES");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);

        // 加密
        byte[] encryptedBytes = cipher.doFinal(plainText.getBytes());
        String encryptedText = Base64.getEncoder().encodeToString(encryptedBytes);
        System.out.println("Encrypted Text: " + encryptedText);

        // 解密
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] decryptedBytes = cipher.doFinal(Base64.getDecoder().decode(encryptedText));
        String decryptedText = new String(decryptedBytes);
        System.out.println("Decrypted Text: " + decryptedText);
    }
}
