package com.office.tools;

import lombok.Data;

import java.io.Serializable;

/**
 *   接口异常返回数据格式
 */
@Data
public class OfficeResult<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 返回代码
	 */
	private Integer code;

	/**
	 * 返回数据对象 data
	 */
	private T data;

	/**
	 * 返回处理消息
	 */
	private String message ;

	/**
	 *
	 */
	private String details;
	/**
	 * 自定义错误提示
	 */
	private String hint ;

	public OfficeResult(Integer code, String message) {
		this.message = message;
		this.code = code;
	}

	public OfficeResult(Integer code, String message, String details, String hint) {
		this.message = message;
		this.details = details;
		this.hint = hint;
		this.code = code;
	}
}