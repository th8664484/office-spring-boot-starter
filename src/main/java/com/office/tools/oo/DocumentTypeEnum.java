package com.office.tools.oo;

/**
 * 文件类型
 */
public enum DocumentTypeEnum {
    Word,
    Cell,
    Slide
}