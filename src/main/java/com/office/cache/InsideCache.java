package com.office.cache;

import cn.hutool.cache.CacheUtil;
import cn.hutool.cache.impl.TimedCache;
import com.office.core.Cache;
import lombok.extern.slf4j.Slf4j;

/*
 * @BelongsProject: office
 * @BelongsPackage: com.office.cache
 * @Author: TongHui
 * @CreateTime: 2023-07-30 17:11
 * @Description: 内部缓存
 * @Version: 1.0
 */
@Slf4j
public class InsideCache implements Cache {


    //时间单位 秒
    private  long timeUnit = 1000;

    //创建缓存，默认 1分钟 过期
    private  TimedCache<String, Object> cache = CacheUtil.newTimedCache(Cache.TIMEOUT);


    public InsideCache(){
        cache.schedulePrune(Cache.TIMEOUT);
    }

    @Override
    public String getCacheName() {
        return this.getClass().getName();
    }

    public  Object get(String key){
        return cache.get(key);
    }

    public  boolean hasKey(String key){
        return cache.get(key) == null ? false : true;
    }

    /**
     *
     *  key
     *  value
     *  time  时间 单位是秒
     */
    public  void set(String key,Object value,long time){
        if (time < timeUnit){
            time = Cache.TIMEOUT;
        }
        cache.put(key, value, time*timeUnit);
        log.info("key:{},value:{},size:{}",key,value,cache.size());
    }

    @Override
    public void set(String key, Object value) {
        cache.put(key, value);
        log.info("key:{},value:{},size:{}", key,value,cache.size());
    }

    public  void remove(String key){
        cache.remove(key);
    }


}
