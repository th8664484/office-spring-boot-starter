package com.office;

import com.office.builder.yml.OOConfigBuilder;
import com.office.cache.InsideCache;
import com.office.config.oo.OnlyProperties;
import com.office.config.wps.WPSBaseInfo;
import com.office.core.Cache;
import com.office.core.CommonConfig;
import com.office.core.MessageHandle;
import com.office.core.SaveFileProcessor;
import com.office.office.oo.OnlyOfficeAPI;
import com.office.office.wps.WPSOfficeAPI;
import com.office.tools.wps.SDKUtil;
import com.sun.istack.internal.NotNull;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.*;


/**
 * 自动配置入口
 */
@Data
@Slf4j
@Configuration
@ComponentScan
@ConfigurationProperties(prefix = "office")
public class OfficeAutoConfiguration implements ApplicationContextAware {

    @NotNull
    private String type;

    @NotNull
    private String downloadFile;

    @NotNull
    private String localhostAddress;

    private Integer histNum;

    private Integer timeout;

    private Long maxSize;


    // wps配置信息
    @NestedConfigurationProperty
    private WPSBaseInfo wps = new WPSBaseInfo();


    // onlyOffice 配置信息
    @NestedConfigurationProperty
    private OnlyProperties oo = new OnlyProperties();

    private ApplicationContext applicationContext;


    @Bean
    @ConditionalOnProperty(prefix = "office", name = "type", havingValue = "wps")
    public WPSOfficeAPI createWPSBean(Cache insideCache){
        if (StringUtils.isEmpty(wps.getAk())){
            throw new IllegalArgumentException("office.wps.ak is empty");
        }
        if (StringUtils.isEmpty(wps.getSk())){
            throw new IllegalArgumentException("office.wps.sk is empty");
        }
        if (StringUtils.isEmpty(wps.getDomainName())){
            throw new IllegalArgumentException("office.wps.domain_name is empty");
        }
        MessageHandle bean = applicationContext.getBean(MessageHandle.class);
        if (bean == null){
            throw new NoSuchBeanDefinitionException(MessageHandle.class.getName());
        }
        log.info("Cache已加载：{}",insideCache.getCacheName());
        CommonConfig commonConfig = getCommonConfig();

        WPSOfficeAPI wpsOfficeAPI = new WPSOfficeAPI(commonConfig);
        wpsOfficeAPI.setMessageHandle(bean);
        wpsOfficeAPI.setCache(insideCache);
        wpsOfficeAPI.setWpsBaseInfo(wps);
        wpsOfficeAPI.setSdkUtil(SDKUtil.create(wps.getDomainName(), wps.getAk(), wps.getSk()));

        log.info("wpsAPI 初始化成功");
        return wpsOfficeAPI;
    }

    @Bean
    @ConditionalOnProperty(prefix = "office", name = "type", havingValue = "oo")
    public OnlyOfficeAPI createOOBean(Cache insideCache){
        if (StringUtils.isEmpty(oo.getDocService())){
            throw new IllegalArgumentException("OnlyOffice服务地址为空");
        }
        if (StringUtils.isEmpty(oo.getCallBackUrl())){
            throw new IllegalArgumentException("OnlyOffice服务回调地址为空");
        }

        SaveFileProcessor bean = applicationContext.getBean(SaveFileProcessor.class);
        if (bean == null){
            throw new NoSuchBeanDefinitionException(SaveFileProcessor.class.getName());
        }

        log.info("Cache已加载：{}",insideCache.getCacheName());

        CommonConfig commonConfig = getCommonConfig();

        OnlyOfficeAPI onlyOfficeAPI = new OnlyOfficeAPI(commonConfig);
        onlyOfficeAPI.setSaveFileProcessor(bean);
        onlyOfficeAPI.setCache(insideCache);
        onlyOfficeAPI.setOnlyProperties(oo);

        OOConfigBuilder ooConfigBuilder = new OOConfigBuilder(onlyOfficeAPI,oo,commonConfig, "onlyOffice.yml");
        ooConfigBuilder.parse();

        log.info("OnlyOffice API 初始化成功");
        return onlyOfficeAPI;
    }



    private CommonConfig getCommonConfig(){
        return new CommonConfig(downloadFile,localhostAddress,histNum,timeout,maxSize);
    }



    @Bean
    @ConditionalOnMissingBean
    public Cache getCche(){
        return new InsideCache();
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
