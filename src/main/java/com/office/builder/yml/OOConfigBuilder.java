package com.office.builder.yml;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.office.config.oo.OnlyProperties;
import com.office.config.oo.Plugins;
import com.office.config.oo.document.DocumentPermission;
import com.office.config.oo.edit.FileCustomization;
import com.office.config.oo.edit.FileEmbedded;
import com.office.core.CommonConfig;
import com.office.core.SaveFileProcessor;
import com.office.office.oo.OnlyOfficeAPI;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;


/*
 * @BelongsProject: office
 * @BelongsPackage: com.office.builder.yml
 * @Author: TongHui
 * @CreateTime: 2024-03-14 09:30
 * @Description: TODO
 * @Version: 1.0
 */

public class OOConfigBuilder {

    private static final Logger log = LoggerFactory.getLogger(OOConfigBuilder.class);

    /**
     * 配置信息
     * @param name
     */
    private String resource;

    private JSONObject configData;

    private OnlyOfficeAPI onlyOfficeAPI;
    private OnlyProperties onlyProperties;
    private CommonConfig configuration;

    public OOConfigBuilder(OnlyOfficeAPI onlyOfficeAPI, OnlyProperties onlyProperties,CommonConfig configuration, String name) {
        this.onlyOfficeAPI = onlyOfficeAPI;
        this.onlyProperties = onlyProperties;
        this.configuration = configuration;
        log.info("--------加载【office.yml】配置文件-------");
        Yaml yaml = new Yaml();
        try {
            this.resource = name;
            InputStream inputStream = OOConfigBuilder.class.getClassLoader().getResourceAsStream(name);
            configData = yaml.loadAs(inputStream, JSONObject.class).getJSONObject("oo");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void parse(){
        // 文档权限信息配置
        setDocument();
        // 编辑操作设置配置
        setEditor();
        //文件转换配置
        onlyOfficeAPI.setConvert(this.configData.getJSONObject("convert"));

        log.info("--------【office.yml】加载完成-------");
    }



    private void setDocument(){
        JSONObject document = this.configData.getJSONObject("document");
        if (null != document || null != document.getJSONObject("permissions")) {
            JSONObject permissions = document.getJSONObject("permissions");
            if (null != permissions.getJSONObject("edit")) {
                DocumentPermission editPermission = JSONUtil.toBean(permissions.getJSONObject("edit").toString(), DocumentPermission.class);
                editPermission.setEdit(true);
                onlyOfficeAPI.setEditPermission(editPermission);
            }
            if (null != permissions.getJSONObject("view")) {
                DocumentPermission viewPermission = JSONUtil.toBean(permissions.getJSONObject("view").toString(), DocumentPermission.class);
                viewPermission.setEdit(false);
                onlyOfficeAPI.setViewPermission(viewPermission);
            }
        }
    }

    private void setEditor(){
        JSONObject editor = this.configData.getJSONObject("editor");
        if (null != editor) {
            FileCustomization customization = JSONUtil.toBean(editor.getJSONObject("customization").toString(), FileCustomization.class);
            onlyOfficeAPI.setCustomization(customization);
        }

        setEmbedded(editor);

        setPlugins(editor);
    }

    private void setEmbedded(JSONObject editor){
        JSONObject embeddedJson = editor.getJSONObject("embedded");
        if (null != embeddedJson) {
            FileEmbedded embedded = JSONUtil.toBean(embeddedJson.toString(), FileEmbedded.class);
            onlyOfficeAPI.setEmbedded(embedded);
        }
    }

    private void setPlugins(JSONObject editor){
        JSONObject pluginsJson = editor.getJSONObject("plugins");
        if (null != pluginsJson) {
            Plugins plugins = JSONUtil.toBean(pluginsJson.toString(), Plugins.class);
            onlyOfficeAPI.setPlugins(plugins);
        }
    }
}
