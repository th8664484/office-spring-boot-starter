package com.office.builder;


import com.office.core.CommonConfig;

/*
 * @BelongsProject: office
 * @BelongsPackage: com.office.builder
 * @Author: TongHui
 * @CreateTime: 2024-03-13 11:08
 * @Description: TODO
 * @Version: 1.0
 */
public abstract class BaseBuilder {
    protected final CommonConfig configuration;

    public BaseBuilder(CommonConfig configuration) {
        this.configuration = configuration;
    }


}
